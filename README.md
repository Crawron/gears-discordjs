# gears-discordjs

This package adds [Discord.js](https://www.npmjs.com/package/discord.js) bindings to [Gears](https://www.npmjs.com/package/@enitoni/gears).

## Installing

You'll need both Discord.js and Gears for this package to work. Install like so:

```
npm install @enitoni/gears @enitoni/gears-discordjs discord.js
```

or if you use Yarn

```
yarn add @enitoni/gears @enitoni/gears-discordjs discord.js
```

## Usage

If you are using TypeScript it is important that you import the classes from this library instead of Gears itself, as this library provides Discord.js types on the generic classes.

Here's an example

```ts
import { Bot, matchPrefixes } from "@enitoni/gears"
import { Adapter, CommandGroup, Command } from "@enitoni/gears-discordjs"

const adapter = new Adapter({
  token: "your secret discord bot token"
})

const command = new Command({
  matcher: matchPrefixes("test"),
  action: context => {
    const { message } = context
    message.channel.send("Test received!")
  }
})

const group = new CommandGroup({
  matcher: matchPrefixes("!"),
  commands: [command]
})

const bot = new Bot({ adapter, group })
bot.start()
```

## Options

The adapter options extend [ClientOptions](https://discord.js.org/#/docs/main/stable/typedef/ClientOptions) and adds two more properties:

- token: string - The Discord bot token used to login
- listenToSelf: boolean (optional) - Set this to true if you want the bot to listen to its own messages, but be aware this can cause an infinite loop if you have commands that respond to things the bot says

## Matchers

This library includes the following matchers:

- matchAuthors - match based on message author
- matchChannels - match based on channel name or id
- matchChannelTypes - match based on channel type
- matchMentions - match based on mentions
- matchMentionsSelf - match if message contains mention of the bot
- matchMessageTypes - match based on message types
- matchNSFW - match if channel is NSFW
- matchRoles - match based on role name or id

## Services

This library includes the following services:

- ConsoleLoggingService - logs info, warnings and errors to console
