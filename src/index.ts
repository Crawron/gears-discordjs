import * as Gears from "@enitoni/gears"
import { Client, ClientOptions, Message } from "discord.js"

export interface AdapterOptions extends ClientOptions {
  token: string
  listenToSelf?: boolean
}

export class Adapter extends Gears.ClientAdapter<Message, Client, AdapterOptions> {
  protected register(options: AdapterOptions, hooks: Gears.AdapterHooks<Message>) {
    const { token, listenToSelf, ...clientOptions } = options
    const client = new Client(clientOptions)

    client.on("message", message => {
      if (client.user.id !== message.author.id || listenToSelf) {
        hooks.message(message)
      }
    })

    client.on("ready", hooks.ready)
    client.on("resume", hooks.ready)
    client.on("disconnect", hooks.unready)
    client.on("error", hooks.error)

    return {
      client,
      methods: {
        start: async () => {
          await client.login(token)
        },
        getMessageContent: (message: Message) => message.content
      }
    }
  }
}

export type Context<S extends object = {}> = Gears.Context<S, Message, Client>
export type Matcher<S extends object = {}> = Gears.Matcher<S, Message, Client>
export type Middleware<S = unknown> = Gears.Middleware<S, Message, Client>

export declare class Command<D = unknown> extends Gears.Command<Message, Client, D> {}

export declare class CommandGroup<D = unknown> extends Gears.CommandGroup<
  Message,
  Client,
  D
> {}

export declare class CommandBuilder<D = unknown> extends Gears.CommandBuilder<
  Message,
  Client,
  D
> {}

export declare class CommandGroupBuilder<D = unknown> extends Gears.CommandGroupBuilder<
  Message,
  Client,
  D
> {}

export declare class Service extends Gears.Service<Message, Client> {}

exports.Command = Gears.Command
exports.CommandGroup = Gears.CommandGroup
exports.CommandBuilder = Gears.CommandBuilder
exports.CommandGroupBuilder = Gears.CommandGroupBuilder
exports.Service = Gears.Service

export * from "./matchers"
export * from "./services"
