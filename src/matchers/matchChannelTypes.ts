import { ChannelType } from "../types"
import { Matcher } from ".."

/**
 * Matches if channel type is one of the types passed to this matcher
 */
export const matchChannelTypes = (...types: ChannelType[]): Matcher => context => {
  const { message } = context
  const type = message.channel.type as ChannelType

  if (types.includes(type)) return context
}
