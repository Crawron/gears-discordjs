import { Matcher } from ".."
import { MessageMentions } from "discord.js"

export type MatchMentionsCallback = (mentions: MessageMentions) => boolean

const defaultCallback: MatchMentionsCallback = mentions => {
  const { channels, members, roles, users, everyone } = mentions
  return !!(channels.size || members.size || roles.size || users.size || everyone)
}

/**
 * Matches if the message satisfies the mention callback passed to this matcher
 * Default is to match if message contains any mention
 */
export const matchMentions = (callback = defaultCallback): Matcher => context => {
  const { message } = context
  const { mentions } = message

  if (callback(mentions)) return context
}
