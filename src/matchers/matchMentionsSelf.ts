import { Matcher } from ".."

/**
 * Matches when this bot is mentioned
 */
export const matchMentionsSelf = (): Matcher => context => {
  const { message, bot } = context
  const { mentions } = message

  const hasMatch = mentions.users.has(bot.client.user.id)
  if (hasMatch) return context
}
