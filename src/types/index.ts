export type ChannelType = "dm" | "group" | "text"

export type MessageType =
  | "DEFAULT"
  | "RECIPIENT_ADD"
  | "RECIPIENT_REMOVE"
  | "CALL"
  | "CHANNEL_NAME_CHANGE"
  | "CHANNEL_ICON_CHANGE"
  | "PINS_ADD"
  | "GUILD_MEMBER_JOIN"
